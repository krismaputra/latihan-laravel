<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
        return view('register');
    }

    public function welcome1(Request $request){
        $fname = $request->fname;
        $lname = $request->lname;
        return view('welcome1' ,compact('fname', 'lname'));
    }
}
