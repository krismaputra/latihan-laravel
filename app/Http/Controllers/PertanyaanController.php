<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
       //dd($request->all()); //untuk mengecek data apakah sudah masuk dan seperti apa
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

       $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
       ]);
        
       return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');

    }

    public function index(){

        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan')); //compact untuk menjadikan array
    }

    public function show($pertanyaan_id){
        $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        // dd($post);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($pertanyaan_id){
        $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();

        return view('pertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);  
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }
}
